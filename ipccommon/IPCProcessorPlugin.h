#pragma once

#include "Connector.h"
#include "ConnectorLocalhost.h"

#include "IPCFactory.h"
#include "google/protobuf/message_lite.h"
#include "IPCProcessorPlugin.pb.h"

/*Concrete fabric for create localhost Processor-Plugin ipc connector (server or client)*/

using namespace Transfer;

namespace strong
{
    typedef Transfer::strong::ConnectorLocalhostServer IPCProcessorPluginLocalhostServer;
    typedef Transfer::strong::ConnectorLocalhostClient IPCProcessorPluginLocalhostClient;

    typedef IPCProcessorPluginLocalhostServer IPCProcessorPluginRemotehostServer;
    typedef IPCProcessorPluginLocalhostClient IPCProcessorPluginRemotehostClient;

    typedef  Transfer::strong::ConnectorServer ProcessorPluginConnectorServer;
    typedef  Transfer::strong::ConnectorClient ProcessorPluginProtoConnectorClient;

    class ConnectorProcessorPluginServerFactory : public GenericObjectFactory<IPCProcessorPluginLocalhostServer, IPCProcessorPluginRemotehostServer, ProcessorPluginConnectorServer>
    {
    public:
        ConnectorProcessorPluginServerFactory()
        {

        }
    };

    class ConnectorProcessorPluginClientFactory : public GenericObjectFactory <IPCProcessorPluginLocalhostClient, IPCProcessorPluginRemotehostClient, ProcessorPluginProtoConnectorClient>
    {
    public:
        ConnectorProcessorPluginClientFactory()
        {

        }
    };
}