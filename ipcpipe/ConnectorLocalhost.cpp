#include <vector>
#include <algorithm>

#include "ConnectorLocalhost.h"

#define SMO_SERVER_ADDRESS_SIZE 64

namespace Transfer
{
    namespace strong
    {
        PeerLocalhostServer::PeerLocalhostServer(const std::string& realTimeServer, const std::string& bigDataServer)
            : PeerLocalhost(realTimeServer, bigDataServer, PeerType::SERVER) //Please use PeerMap parameter instead of strings
        {

        }


        PeerLocalhostClient::PeerLocalhostClient(const std::string& realTimeServer, const std::string& bigDataServer) 
            : PeerLocalhost(realTimeServer, bigDataServer, PeerType::CLIENT) //Please use PeerMap parameter instead of strings
        {

        }

        ConnectorLocalhost::ConnectorLocalhost(const std::string& ipcNamePrefix, const std::string& ipcName) 
            : ipcArbitName(ipcNamePrefix + ipcName)
        {
        }

        ConnectorLocalhostServer::ConnectorLocalhostServer(const std::string& ipcNamePrefix, const std::string& ipcName) 
            : ConnectorLocalhost(ipcNamePrefix, ipcName)
        {

        }

        /*create localhost ipc*/
        Peer* ConnectorLocalhostServer::Create()
        {
            /*create helper object invoking ConnectorHelperLocalhost::GetServerInstance with arbitr name (name of shared memory object)*/
            this->connectorHelperLocalhostServer = ConnectorHelperLocalhost::GetServerInstance(ipcArbitName);

            /*generating ipc name for real time and big ibject and store it in the shared memory (for ipc client)*/
            std::string realTimeServer = this->connectorHelperLocalhostServer->GenerateIPCName();
            std::cout << "realTimeServer = " << realTimeServer << std::endl;
            std::string bigDataServer = realTimeServer + std::string("_");

            /*create server ipc peer*/
            this->server = std::make_unique <PeerLocalhostServer>(realTimeServer, bigDataServer);

            return dynamic_cast <Peer*> (this->server.get());
        }

        ConnectorLocalhostClient::ConnectorLocalhostClient(const std::string& ipcNamePrefix, const std::string& ipcName) 
            : ConnectorLocalhost(ipcNamePrefix, ipcName)
        {

        }

        Peer* ConnectorLocalhostClient::Open()
        {
            /*create client peer*/
            this->connectorHelperLocalhostClient = ConnectorHelperLocalhost::CreateClientInstance(ipcArbitName);

            std::string realTimeServer = this->connectorHelperLocalhostClient->GetIPCName();
            std::cout << "realTimeServer = " << realTimeServer << std::endl;
            std::string bigDataServer = realTimeServer + std::string("_");

            this->client = std::make_unique <PeerLocalhostClient>(realTimeServer, bigDataServer);
            return dynamic_cast <Peer*> (this->client.get());
        }

    }
}
