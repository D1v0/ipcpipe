#pragma once

/*defs for platform specific*/

#include <memory>
#include <thread>

#ifdef _WIN32
#include "boost/interprocess/managed_windows_shared_memory.hpp"
#define BOOST_INTERPROCESS_SHARED_DIR_PATH "c:/ProgramData/boost_interprocess"
typedef boost::interprocess::managed_windows_shared_memory shared_memory_t;
#else
#define BOOST_INTERPROCESS_SHARED_DIR_PATH "/tmp"
#include "boost/interprocess/managed_xsi_shared_memory.hpp"
typedef boost::interprocess::managed_xsi_shared_memory shared_memory_t;
#endif
#include "boost/interprocess/ipc/message_queue.hpp"

typedef std::unique_ptr<shared_memory_t> SharedMemory;
typedef std::unique_ptr<boost::interprocess::message_queue> MessageQueue;