#pragma once

#include <memory>
#include <string>
#include "IPCLocalhost.h"

/*IPCPipe interface for client and server*/

namespace ipc
{
    /*Generic pipe interface*/ 
    class Pipe
    {
    public:
        /*send data*/
        virtual void Send(const std::string&  dataToSend) = 0;
        /*recv data*/
        virtual bool Recv(std::string&  dataToRecv) = 0;

        /*create pipe (for server side)*/
        virtual void Create(const std::string& name, size_t size) = 0;
        /*open pipe (for client side)*/
        virtual void Open(const std::string& name, size_t size) = 0;

        virtual int GetErrorCode() = 0;

    protected:
        bool isServer;
        bool connected;
    };

    /*Interface of the localhost IPC based on the Pipe*/
    class IPCPipe
        : public IPCLocalhost
    {
    public:
        virtual ~IPCPipe() = 0
        {

        }
    };

    /*Server side implementation localhost IPC*/
    class IPCPipeServer
        : public IPCPipe
    {
    public:
        IPCPipeServer(const std::string& name, size_t size);
        virtual ~IPCPipeServer();

        void Send(const std::string& message, bool async = false) const override; //write in 
        bool RecvNow(std::string& message) const override;
        void SetRecvCallback(RecvCallback recvCallback) override;

    private:
        void RecvThread();

        //Full duplex transfer, one peer for send, another for recv
        std::unique_ptr<Pipe> pipeServerIn; 
        std::unique_ptr<Pipe> pipeServerOut;
    };

    /*Client side implementation localhost IPC*/
    class IPCPipeClient
        : public IPCPipe
    {
    public:
        IPCPipeClient(const std::string& name, size_t size);
        virtual ~IPCPipeClient();

        void Send(const std::string& message, bool async = false) const override; //write in 
        bool RecvNow(std::string& message) const override;
        void SetRecvCallback(RecvCallback recvCallback) override;

    private:
        void RecvThread();
        
        //Full duplex transfer, one peer for send, another for recv
        std::unique_ptr<Pipe> pipeClientIn;
        std::unique_ptr<Pipe> pipeClientOut;
    };
};