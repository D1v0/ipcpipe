#pragma once

#include "Connector.h"
#include "ConnectorLocalhost.h"
#include "IPCFactory.h"

#include "RenderTransfer.pb.h"
#include "google/protobuf/message_lite.h"

/*Concrete fabric for create localhost Plugin-Render ipc connector (server or client)*/

using namespace Transfer;

namespace strong
{
    typedef Transfer::strong::ConnectorLocalhostServer IPCPluginRenderLocalhostServer;

    typedef Transfer::strong::ConnectorLocalhostClient IPCPluginRenderLocalhostClient;

    typedef Transfer::strong::ConnectorServer PluginRenderConnectorServer;
    typedef Transfer::strong::ConnectorClient PluginRenderConnectorClient;

    class ConnectorPluginRenderServerFactory : public GenericObjectFactory <IPCPluginRenderLocalhostServer, IPCPluginRenderLocalhostServer, PluginRenderConnectorServer>
    {
    public:
        ConnectorPluginRenderServerFactory()
        {

        }
    };

    class ConnectorPluginRenderClientFactory : public GenericObjectFactory <IPCPluginRenderLocalhostClient, IPCPluginRenderLocalhostClient, PluginRenderConnectorClient>
    {
    public:
        ConnectorPluginRenderClientFactory()
        {

        }
    };
}