#include "IPCFactory.h"
#include "IPCPluginRender.h"
#include "IPCProcessorPlugin.h"

/*Simple test module*/

void TestInit()
{
    /*create concrete factory*/
    auto connectorPluginRenderServerFactory = ConnectorPluginRenderServerFactory::Instance();
    /*create local peer*/
    auto connectorLocalHost = connectorPluginRenderServerFactory->getLocal <std::string, std::string>("host", "render");

    /*create peer for server side*/
    auto peerLocalHostServer = connectorLocalHost->Create();
    peerLocalHostServer->Send(dataToSend, Transfer::SendDataType::REAL_TIME, false/*async*/);
    
    /*create peer for client*/
    auto peerLocalHostClient = connectorLocalHost->Open();
    peerLocalHostClient->RecvNow(dataToReceive, Transfer::SendDataType::REAL_TIME);
    
}