#pragma once
#include "IPCLocalhost.h"

#include <string>
#include <random>
#include <functional>

#include "IPCLocalhostDefs.h"
#include "IPCLocalhostData.h"

/*class for generate random ipc name and putting them into shared memory*/

class RandomGenerator
{
public:
    explicit RandomGenerator();
    std::string GenerateRandomString(size_t length);

private:
    std::default_random_engine m_random_engine;
    std::uniform_int_distribution<> m_dist;
};

/*helper class for a generation unique names for local ipc*/
class ConnectorHelperLocalhost
{
public:
    ConnectorHelperLocalhost(std::string name);
    virtual ~ConnectorHelperLocalhost();

    void Create(size_t size);
    void Open();

    std::string GenerateIPCName();
    std::string GetIPCName();
    bool IPCNamePresent() const;

    static ConnectorHelperLocalhost* GetServerInstance(const std::string& name);
    static std::unique_ptr<ConnectorHelperLocalhost> CreateClientInstance(const std::string& name);

protected:
    std::string name;
    char* ipcName;
    int* ipcNameLen;
    cond_t* ipcNamePresentCondVar;
    mutex_t* mutex;
    bool* ipcNamePresent;

    SharedMemory sharedMemory_;
    ipc::IPCLocalhostData* data;
};

