#include "ConnectorHelperLocalhost.h"
#include <vector>
#include <algorithm>

/*helper object - it is shared memory object with predefined name, which generate random ipc name and store it*/

using namespace boost::interprocess;

static std::vector<char> g_random_chars = { '0','1','2','3','4','5','6','7','8','9',
'A','B','C','D','E','F','G','H','I','J',
'K','L','M','N','O','P','Q','R','S','T',
'U','V','W','X','Y','Z','a','b','c','d',
'e','f','g','h','i','j','k','l','m','n',
'o','p','q','r','s','t','u','v','w','x',
'y','z' };



RandomGenerator::RandomGenerator()
    : m_random_engine(std::random_device{}()),
    m_dist(0, (int)g_random_chars.size() - 1)
{
}

std::string RandomGenerator::GenerateRandomString(size_t length)
{
    std::string str(length, 0);
    std::generate_n(str.begin(), length, [this]() { return g_random_chars[m_dist(m_random_engine)]; });
    return str;
}

ConnectorHelperLocalhost::ConnectorHelperLocalhost(std::string name) //:
    :name(name)
{
}

ConnectorHelperLocalhost::~ConnectorHelperLocalhost()
{
}

/*create server instance*/
void ConnectorHelperLocalhost::Create(size_t size)
{
    this->sharedMemory_.reset(new shared_memory_t(create_only, name.c_str(), size));
    this->ipcName = this->sharedMemory_->construct <char>("ipcName")[255]();
    this->ipcNameLen = this->sharedMemory_->construct <int>("ipcNameLen")(0);
    this->ipcNamePresentCondVar = this->sharedMemory_->construct <cond_t>("ipcNamePresentCV")();
    this->ipcNamePresent = this->sharedMemory_->construct <bool>("ipcNamePresent")(false);
    this->mutex = this->sharedMemory_->construct <mutex_t>("mutex")();
}
/*create client instance*/
void ConnectorHelperLocalhost::Open()
{
    //ipc::IPCLocalhost::Open();
    this->sharedMemory_.reset(new shared_memory_t(open_only, name.c_str()));
    this->ipcName = this->sharedMemory_->find<char>("ipcName").first;
    this->ipcNameLen = this->sharedMemory_->find<int>("ipcNameLen").first;
    this->ipcNamePresentCondVar = this->sharedMemory_->find<cond_t>("ipcNamePresentCV").first;
    this->ipcNamePresent = this->sharedMemory_->find<bool>("ipcNamePresent").first;
    this->mutex = this->sharedMemory_->find <mutex_t>("mutex").first;
}
/*generate name and store it*/
std::string ConnectorHelperLocalhost::GenerateIPCName()
{
    boost::interprocess::scoped_lock<mutex_t> lock(*this->mutex);
    this->ipcNamePresentCondVar->wait(lock, [this]() 
    {
        return !*this->ipcNamePresent; 
    });
    lock.unlock();


    RandomGenerator ipcNameGenerator;
    std::string ipcName = ipcNameGenerator.GenerateRandomString(7);

    memcpy(this->ipcName, ipcName.data(), ipcName.size());
    *this->ipcNameLen = ipcName.size();


    *this->ipcNamePresent = true;
    this->ipcNamePresentCondVar->notify_all();

    return ipcName;
}
/*get name (for client side)*/
std::string ConnectorHelperLocalhost::GetIPCName()
{
    boost::interprocess::scoped_lock<mutex_t> lock(*this->mutex);
    this->ipcNamePresentCondVar->wait(lock, [this]()
    {
        return *this->ipcNamePresent;
    });
    lock.unlock();

    std::string ipcName(this->ipcName, *this->ipcNameLen);
    
    return ipcName;
}

bool ConnectorHelperLocalhost::IPCNamePresent() const
{
    return *this->ipcNamePresent;
}

/*server instance for a shared memory*/
ConnectorHelperLocalhost* ConnectorHelperLocalhost::GetServerInstance(const std::string& name)
{
    static std::unique_ptr <ConnectorHelperLocalhost> instance;
    if (!instance)
    {
        instance = std::make_unique <ConnectorHelperLocalhost>(name);
        instance->Create(1024);
    }
    return instance.get();
}

/*client instance for a shared memory*/
std::unique_ptr<ConnectorHelperLocalhost> ConnectorHelperLocalhost::CreateClientInstance(const std::string& name)
{
    std::unique_ptr <ConnectorHelperLocalhost> instance;
    instance = std::make_unique <ConnectorHelperLocalhost>(name);
    instance->Open();
    return std::move(instance);
}