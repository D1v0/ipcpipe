#pragma once

#include "boost/interprocess/sync/interprocess_mutex.hpp"
#include "boost/interprocess/sync/interprocess_condition.hpp"
#include "boost/interprocess/sync/interprocess_mutex.hpp"
#include "boost/interprocess/sync/interprocess_condition.hpp"

/*Shared memory data (for ipc name generator)*/

typedef boost::interprocess::interprocess_mutex mutex_t;
typedef boost::interprocess::interprocess_condition cond_t;

namespace ipc
{
    struct IPCLocalhostData
    {
        mutex_t mutex;

        size_t handshakeField; //hash for handshake
        bool clientJoin = false; //true if client joined
        cond_t clientJoined; //cond_var for client connection, time to check handshakeField

        bool clientClose = false;
        cond_t clientClosed;

        bool clientReady = false;
        cond_t clientReadyRead;
        bool serverWritten = false;
        cond_t serverDataWritten;

        bool serverReady = false;
        cond_t serverReadyRead;
        bool clientWritten = false;
        cond_t clientDataWritten;

        IPCLocalhostData() = default;
        ~IPCLocalhostData() = default;
    };
}