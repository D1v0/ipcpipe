#include "IPCPipe.h"

#ifdef _WIN32
#include <Windows.h>
#else

#endif // _WIN32

/*IPCPipe implementation for WIN32*/

namespace ipc
{

    class WinPipe : public Pipe
    {
    public:
        WinPipe(bool inDirection) //inDirection = true, for Server: In (Recv), Out (Send)
            : inDirection (inDirection)
            , name ("\\\\.\\pipe\\")
            , size (0)
            , pipe (INVALID_HANDLE_VALUE)
            , errorCode (0)
            , buffer (nullptr)
        {
            this->isServer = false;
            this->connected = false;
        }
        virtual ~WinPipe()
        {
            delete[] buffer;
            CloseHandle(this->pipe);
        }

        void Send(const std::string&  dataToSend) override
        {
            if (this->isServer && !this->connected)
                this->connected = ConnectNamedPipe(this->pipe, NULL);

            BOOL success = FALSE;
            DWORD writtenBytes = 0;

            success = WriteFile( this->pipe, dataToSend.c_str(), dataToSend.length(), &writtenBytes, NULL);

            if (!success)
                this->errorCode = GetLastError();
        }

        bool Recv(std::string&  dataToRecv) override
        {
            if (this->isServer)
                ConnectNamedPipe(this->pipe, NULL);

            BOOL success = FALSE;
            
            DWORD readedBytes = 0;

            success = ReadFile(this->pipe, buffer, this->size,  &readedBytes,  NULL);

            if (success)
            {
                dataToRecv.assign(buffer, readedBytes);
                return true;
            }
            else
            {
                this->errorCode = GetLastError();
                return false;
            }

        }

        void Create(const std::string& name, size_t size) override
        {
            this->name.append(name);
            this->size = size;

            if (this->inDirection)
                this->pipe = CreateNamedPipeA(this->name.c_str(), PIPE_ACCESS_INBOUND, PIPE_TYPE_BYTE | PIPE_READMODE_BYTE | PIPE_WAIT, PIPE_UNLIMITED_INSTANCES, size, size, 0, nullptr);
            else
                this->pipe = CreateNamedPipeA(this->name.c_str(), PIPE_ACCESS_OUTBOUND, PIPE_TYPE_BYTE | PIPE_READMODE_BYTE | PIPE_WAIT, PIPE_UNLIMITED_INSTANCES, size, size, 0, nullptr);

            this->errorCode = GetLastError();

            if (pipe != INVALID_HANDLE_VALUE)
            {
                this->buffer = new char[this->size];
            }

            this->isServer = true;
        }
        
        void Open(const std::string& name, size_t size) override
        {
            this->name.append(name);
            this->size = size;

            if (this->inDirection)
                this->pipe = CreateFileA(this->name.c_str(), GENERIC_READ, 0, nullptr, OPEN_EXISTING, 0, nullptr);
            else
                this->pipe = CreateFileA(this->name.c_str(), GENERIC_WRITE, 0, nullptr, OPEN_EXISTING, 0, nullptr);

            this->errorCode = GetLastError();

            if (pipe != INVALID_HANDLE_VALUE)
            {
                this->buffer = new char[this->size];
            }
        }

        int GetErrorCode() override
        {
            return static_cast<int>(this->errorCode);
        }

    private:
        bool inDirection;
        std::string name;
        std::size_t size;
        HANDLE pipe;
        DWORD errorCode;
        char* buffer;
    };

#ifdef _WIN32
    typedef WinPipe OSPipe;
#else

#endif

    IPCPipeServer::IPCPipeServer(const std::string & name, size_t size)
        : pipeServerIn (std::make_unique <OSPipe> (true))
        , pipeServerOut (std::make_unique <OSPipe>(false))
    {
        std::string nameIn(name);
        nameIn.append("_in");
        this->pipeServerIn->Create(nameIn, size);

        std::string nameOut(name);
        nameOut.append("_Out");
        this->pipeServerOut->Create(nameOut, size);
    }

    IPCPipeServer::~IPCPipeServer()
    {
    }

    void IPCPipeServer::Send(const std::string & message, bool async) const
    {
        this->pipeServerOut->Send(message);
    }

    bool IPCPipeServer::RecvNow(std::string & message) const
    {
        return this->pipeServerIn->Recv(message);
    }

    void IPCPipeServer::SetRecvCallback(RecvCallback recvCallback)
    {

    }

    void IPCPipeServer::RecvThread()
    {

    }

    IPCPipeClient::IPCPipeClient(const std::string& name, size_t size)
        : pipeClientIn (std::make_unique <OSPipe>(true))
        , pipeClientOut (std::make_unique <OSPipe>(false))
    {
        std::string nameIn(name);
        nameIn.append("_Out");
        this->pipeClientIn->Open(nameIn, size);

        std::string nameOut(name);
        nameOut.append("_In");
        this->pipeClientOut->Open(nameOut, size);
    }

    IPCPipeClient::~IPCPipeClient()
    {

    }

    void IPCPipeClient::Send(const std::string & message, bool async) const
    {
        this->pipeClientOut->Send(message);
    }

    bool IPCPipeClient::RecvNow(std::string & message) const
    {
        return this->pipeClientIn->Recv(message);
    }

    void IPCPipeClient::SetRecvCallback(RecvCallback recvCallback)
    {

    }

    void IPCPipeClient::RecvThread()
    {
    }

}