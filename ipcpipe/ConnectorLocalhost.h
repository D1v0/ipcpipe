#pragma once

#include <map>
#include <mutex>
#include <string>
#include <random>
#include <functional>

#include "Connector.h"
#include "PeerLocalhost.h"
#include "ConnectorHelperLocalhost.h"

/*Sets of the peers and connectors for localhost ipc*/

namespace Transfer
{
    namespace strong
    {
        class PeerLocalhostServer: public PeerLocalhost /*closed class, incasulated localhost ipc*/
        {
        public:
            /*ctor with two names for two kind of localhost ipc - for realtime data and big data*/
            PeerLocalhostServer(const std::string& realTimeServer, const std::string& bigDataServer);
            PeerLocalhostServer() = default;
            virtual ~PeerLocalhostServer() = default;
        };

        class PeerLocalhostClient: public PeerLocalhost
        {
        public:
            PeerLocalhostClient(const std::string& realTimeServer, const std::string& bigDataServer);
            PeerLocalhostClient() = default;
            virtual ~PeerLocalhostClient() = default;
        };

        /*class for connecting two localhost peers of ipc*/
        class ConnectorLocalhost
        {
        public:
            ConnectorLocalhost() = default;
            virtual ~ConnectorLocalhost() = default;
            /*prefix for a some kind of "type" ipc - GPU, PLUGIN, PROCESSOR, etc*/
            ConnectorLocalhost(const std::string& ipcNamePrefix, const std::string& ipcName);

        protected:
            std::string ipcArbitName;
        };

        class ConnectorLocalhostServer: public ConnectorLocalhost, public ConnectorServer, public ConnectorFabric
        {
        public:
            ConnectorLocalhostServer() = default;
            ConnectorLocalhostServer(const std::string& ipcNamePrefix, const std::string& ipcName);
            virtual ~ConnectorLocalhostServer() = default;

            Peer* Create() override;

        private:
            ConnectorHelperLocalhost* connectorHelperLocalhostServer;
        };

        class ConnectorLocalhostClient: public ConnectorLocalhost, public ConnectorClient, public ConnectorFabric
        {
        public:
            ConnectorLocalhostClient() = default;
            ConnectorLocalhostClient(const std::string& ipcNamePrefix, const std::string& ipcName);
            virtual ~ConnectorLocalhostClient() = default;

            Peer* Open() override;

        private:
            std::unique_ptr <ConnectorHelperLocalhost> connectorHelperLocalhostClient;
        };
    }
}