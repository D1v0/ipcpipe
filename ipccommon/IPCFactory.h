#pragma once

#include <map>
#include <memory>

#include "Connector.h"

/*Generic factory for creating pipes endpoint (server or client, remote or local)*/

enum class IPCType
{
    LocalHost,
    RemoteHost
};

template<class LocalConnectorFactory, class RemoteConnectorFactory, class ConnectorType>
class GenericObjectFactory
{
private:
    std::map<IPCType, Transfer::ConnectorFabric*> classes;

public:
    GenericObjectFactory()
    {
        this->add <LocalConnectorFactory>(IPCType::LocalHost);
        this->add <RemoteConnectorFactory>(IPCType::RemoteHost);
    }

    template<typename ... Args>
    std::unique_ptr<ConnectorType> getLocal(Args... args)
    {
        return classes[IPCType::LocalHost]->CreateInstance <LocalConnectorFactory>(args...);
    }

    template<typename ... Args>
    std::unique_ptr<ConnectorType> getRemote(Args... args)
    {
        return classes[IPCType::RemoteHost]->CreateInstance <RemoteConnectorFactory>(args...);
    }

    static GenericObjectFactory* Instance()
    {
        static GenericObjectFactory factory;
        return &factory;
    }

private:
    template<class Derived>
    void add(IPCType id)
    {
        classes[id] = new Derived;
    }
};

